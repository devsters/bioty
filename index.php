<!DOCTYPE html>
<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="css/hayacss.css" >
<script type="text/javascript" src="js/haya.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

     <title>Home</title>
     <meta charset="utf-8">
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" href="css/style.css">
     <link rel="stylesheet" href="css/slider.css">
     <link rel="stylesheet" href="sitedu.css">
     <script src="js/jquery.easing.1.3.js"></script>
     <script src="js/jquery-migrate-1.1.1.js"></script>
     <script src="js/superfish.js"></script>

     <script src="js/tms-0.4.1.js"></script>
     <script src="js/jquery.carouFredSel-6.1.0-packed.js"></script>
     <script>
      $(window).load(function(){
		  $('.slider')._TMS({
			show:0,
			pauseOnHover:false,
			prevBu:'.prev',
			nextBu:'.next',
			playBu:false,
			duration:800,
			preset:'fade',
			easing:'easeOutQuad', 
			pagination:true,//'.pagination',true,'<ul></ul>'
			pagNums:false,
			slideshow:8000,
			numStatus:false,
			banners:'fade',
			waitBannerAnimation:false,
			progressBar:false
		  })  
      });
      
	  $(window).load (
		 function(){$('.carousel1').carouFredSel({auto: false,prev: '.prev1',next: '.next1', width: 960, items: {
			 visible : {min: 4, max: 4},
		  }, 
		  responsive: false, 
		  scroll: 1, 
		  mousewheel: false,
		  swipe: {onMouse: false, onTouch: false}});
	  });      

     </script>
     <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
     <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <link rel="stylesheet" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body>
<!--==============================header=================================-->
     <?php 
                    include("connexion.php");
					$req="select nom_savon from savon ";
                    $result=mysql_query ($req);
					$req_huile="select nom_savon from savon where matiere = 'Huile d`olive'";
					$res_huile=mysql_query ($req_huile);
					$req_argan="select nom_savon from savon where matiere = 'Argan'";
					$res_argan=mysql_query ($req_argan);
					$req_miel="select nom_savon from savon where matiere = 'Miel'";
					$res_miel=mysql_query ($req_miel);
					$req_spi="select nom_savon from savon where matiere = 'Spiruline'";
					$res_spi=mysql_query ($req_spi);
					$req_fleur="select nom_savon from savon where matiere = 'Fleur d`oranger'";
					$res_fleur=mysql_query ($req_fleur);
                    ?>
<header>

	<div class="container_12">
		<div class="grid_12">
           <h1><a href="index.php"><img src="images/logo.png" alt="BIZZ" id="logo"></a></h1>
           <a href="form.html" id="sin">S'inscrire</a>
           <a href="php/Main_menu.php" id="data">Base de donné</a>
           <a href="php/newsletter.html" >News letter</a>
		</div>
    </div>
<div class="content">
<ul class="jetmenu blue">
<li class="active">
<a href="#">Home</a></li>
<li><a href="index-2.php">Produit</a>
<div class="megamenu full-width">
<div class="row">
<div class="col1">
<ul>
<li class="title">
<h5>Nos savon</h5></li>
<?php while($save = mysql_fetch_array($result)) { ?>
<li><a href="#"><?php echo($save["nom_savon"]); ?></a></li>
<?php }?>
</ul></div>
<div class="col1">
<ul>
<li class="title">
<h5><a href="php/produit.php?idp=Huile d`olive">Huile d`olive</a></h5></li>
<?php while($save_huile = mysql_fetch_array($res_huile)) { ?>
<li><a href="#"><?php echo($save_huile["nom_savon"]); ?></a></li>
<?php } ?>
</ul></div>
<div class="col1">
<ul>
<li class="title">
<h5><a href="php/produit.php?idp=Argan">Argan</a></h5></li>
<?php while($save_argan = mysql_fetch_array($res_argan)) { ?>
<li><a href="#"><?php echo($save_argan["nom_savon"]); ?></a></li>
<?php } ?>
</ul></div>
<div class="col1">
<ul>
<li class="title">
<h5>Miel</h5></li>
<?php while($save_miel = mysql_fetch_array($res_miel)) { ?>
<li><a href="#"><?php echo($save_miel["nom_savon"]); ?></a></li>
<?php } ?>
</ul></div>
<div class="col1">
<ul><li class="title">
<h5><a href="php/produit.php?idp=Spiruline">Spiruline</a></h5></li>
<?php while($save_spi = mysql_fetch_array($res_spi)) { ?>
<li><a href="#"><?php echo($save_spi["nom_savon"]); ?></a></li>
<?php } ?>
</ul></div>
<div class="col1"><ul><li class="title">
<h5>Fleur d`oranger</h5></li>
<?php while($save_fleur = mysql_fetch_array($res_fleur)) { ?>
<li><a href="#"><?php echo($save_fleur["nom_savon"]); ?></a></li>
<?php } ?>
</ul></div>

</div></div></li>

<li><a href="#">Dropdown</a>
<ul class="dropdown">
<li><a href="#">Development</a></li>
<li><a href="#">Hosting</a></li>
<li><a href="#">Design</a><ul class="dropdown">
<li><a href="#">Graphics</a></li>
<li><a href="#">Vectors</a></li>
<li><a href="#">Photoshop</a><ul class="dropdown">
<li><a href="#">Photo editing</a></li>
<li><a href="#">Business cards</a></li>
<li><a href="#">Websites</a></li>
<li><a href="#">Illustrations</a></li></ul></li>
<li><a href="#">Fonts</a></li></ul></li>
<li><a href="#">Consulting</a></li></ul></li>

<li><a href="#">Nature</a><div class="megamenu half-width"><div class="row">
<div class="col2"><img src="huile.jpg" alt="image" /></div><div class="col4">Lorem ipsum dolor sit amet, consectetur meti facilisi a adipiscing elit. AmetCurabitur nec lorem alty amet massalit fermentum massa eti. Nulla facilisi. Phasellus in rutrum felis, sed fringilla turpis. Etiam puris noiteki dignissim puruseli tempus, commodo neque eu. Dui curabitur cursus feugiatullamcorper a nibh nec, ost.</div></div><div class="row"><div class="col2"><img src="orange.jpg" alt="image" /></div><div class="col4">Aliquam erat volutpat. Nulla nec justo dui. Aeneanoi atet accumsan egestas tortor at lacinia. Pellentesque netus habitant morbi tristique senectus et netus etor egestasio malesuada fames ac turpis egestas. Utilo pellentesquetii laoreet leo eget pretium. Integeretus eleifend et mi at velo elementum. Maecenas vitaelo. </div></div><div class="row"><div class="col2"><img src="argile.jpg" alt="image" /></div><div class="col4">Lorem ipsum dolor sit amet, consectetur meti facilisi a adipiscing elit. AmetCurabitur nec lorem alty amet massalit fermentum massa eti. Nulla facilisi. Phasellus in rutrum felis, sed fringilla turpis. Etiam puris noiteki dignissim puruseli tempus, commodo neque eu. Dui curabitur cursus feugiatullamcorper a nibh nec, ost. </div></div></div></li><li><a href="#">Grid system</a><div class="megamenu full-width"><div class="row"</div>
</li><li class="right"><a href="#">Contact</a><div class="megamenu half-width"><div class="row"><div class="col6"><form method="post" /><input type="text" name="name" placeholder="Name" /><input type="text" name="email" placeholder="Email" /><textarea name="message" rows="8" placeholder="Message"></textarea><input type="submit" value="Send" /></form></div></div></div></li></ul></i></p></div>
</header>

<!--=======content================================-->
<div id="content"><div class="ic">More Website Templates @ TemplateMonster.com - July 22, 2013!</div>
	<div class="slider-relative">
        <div class="slider-block">
            <div class="slider">
                <ul class="items">
                    <li><img src="images/slider-1.png" alt="">
                        <div class="banner">Qunad le bain se fait gourmand</div>
                    </li>
                    <li><img src="images/slider-2.png" alt="">
                        <div class="banner">Just Bio</div>
                    </li>
                    <li><img src="images/slider-3.png" alt="">
                        <div class="banner">Only SABOON</div>
                    </li>
                    <li><img src="images/slider-4.png" alt="">
                        <div class="banner">Production diary , haute calification</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
	<div class="container_12">
		<div class="grid_3 box-1">
        	<div class="wrap">
                <img src="images/page1-img1.png" alt="">
                <h4>Standards</h4>
            </div>
            At vero eos et accusamus et iusto odio dignissimos ducimus qui bl abd gdihtii praesentium voluptatum deenir rf uiti jll atque. Corrupti quos doloregts.
		</div>
        <div class="grid_3 box-1">
        	<div class="wrap">
                <img src="images/page1-img2.png" alt="">
                <h4>Principles</h4>
            </div>
            Gthoie eos et accusamus et iusto odio dignissimos ducimus qui bl abd gdihtii praesentium voluptatum deenir rf uiti jll atque. Mjhof sdaw doloregts.
		</div>
        <div class="grid_3 box-1">
        	<div class="wrap">
                <img src="images/page1-img3.png" alt="">
                <h4>Experience</h4>
            </div>
            Loas jift accusamus et iusto odio dniloit
ssimos ducimus qui bl abd gdihtiijolwer praesentium voluptatum deenir rf uiti jlol atque. Gfasdh quos doloregts.
		</div>
        <div class="grid_3 box-1">
        	<div class="wrap">
                <img src="images/page1-img4.png" alt="">
                <h4>Client Reviews</h4>
            </div>
            Owasd et accusamus et iusto odiomulo dignissimos ducimus qui bl abd gdihtiie praesentium voluptatum deenir rf uiti jllti atque. Ckloerupti quos doloregts.
		</div>
        <div class="clear"></div>
	</div>
    <div class="bg-1">
        <div class="container_12">
            <div class="grid_12">
            	<div class="carousel">
                    <a href="#" class="prev1"></a><a href="#" class="next1"></a>
                    <div class="carousel_div">
                        <ul class="carousel1">
                          <li>
                            <img src="images/page1-img5.jpg" alt="">
                            <h4>Savon huile d'olive</h4>
                            At vero eos et accusamus et iusto odioll dignissimos ducimus qui bl abd gdihtiiti praesentium voluptatum deenir rf uiti jlle atque. Corrupti quos doloregts et quaso molestias excepturi sint  hip occaecatiw. At vero eos et  hhj  jk kjusamutrelomiker
    oluty gtrew lokiju qwertynomi. <br>
                            <a href="#" class="btn">Plus</a>           
                          </li>
                          <li>
                            <img src="images/page1-img6.jpg" alt="">
                            <h4>Savon authentique</h4>
                            Aolity vero eos et accusamus et iustokill odio dignissimos ducimus qui bl abduyl gdihtii praesentium voluptatum deenir rfi uiti jll atque. Dertorrupti quos doloregtse et quas molestias excepturi sint  hipterq occaecati. Ot vero eos et  hhj  jklomi poli kjusamu s et iusto odio dignissm.           <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                          <li>
                            <img src="images/page1-img7.jpg" alt="">
                            <h4>Savon parfumé</h4>
                            Tro eos et accusamus et iusto odioyrew dignissimos ducimus qui bl abd gdihtiitt praesentium voluptatum deenir rf uiti jlle atque. Holrrupti quos doloregts et quasi molestias excepturi sint  hip occaecatill. Ght vero eos et  hhj  jk kjusamuremmo li                              s et iusto odio dignissms ucimus.         <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                          <li>
                            <img src="images/page1-img8.jpg" alt="">
                            <h4>Coffret marriage</h4>
                            Hfvero eos et accusamus et iusto odiotri dignissimos ducimus qui bl abd gdihtiin praesentium voluptatum deenir rf uiti jllo atque. Dorrupti quos doloregts et quase molestias excepturi sint  hip occaecatib. Rt vero eos et  rewlo jk kjusamu wertilo masdes et iusto odio asgnissme.          <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                          <li>
                            <img src="images/page1-img5.jpg" alt="">
                            <h4>Savon huile d'olive</h4>
                            At vero eos et accusamus et iusto odioll dignissimos ducimus qui bl abd gdihtiiti praesentium voluptatum deenir rf uiti jlle atque. Corrupti quos doloregts et quaso molestias excepturi sint  hip occaecatiw. At vero eos et  hhj  jk kjusamutrelomiker
    oluty gtrew lokiju qwertynomi. <br>
                            <a href="#" class="btn">Plus</a>           
                          </li>
                          <li>
                            <img src="images/page1-img6.jpg" alt="">
                            <h4>Savon authentique</h4>
                            Aolity vero eos et accusamus et iustokill odio dignissimos ducimus qui bl abduyl gdihtii praesentium voluptatum deenir rfi uiti jll atque. Dertorrupti quos doloregtse et quas molestias excepturi sint  hipterq occaecati. Ot vero eos et  hhj  jklomi poli kjusamu s et iusto odio dignissm.           <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                          <li class="grid_3">
                            <img src="images/page1-img7.jpg" alt="">
                            <h4>Savon parfumé</h4>
                            Tro eos et accusamus et iusto odioyrew dignissimos ducimus qui bl abd gdihtiitt praesentium voluptatum deenir rf uiti jlle atque. Holrrupti quos doloregts et quasi molestias excepturi sint  hip occaecatill. Ght vero eos et  hhj  jk kjusamuremmo li                              s et iusto odio dignissms ucimus.         <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                          <li>
                            <img src="images/page1-img8.jpg" alt="">
                            <h4>Coffret marriage</h4>
                            Hfvero eos et accusamus et iusto odiotri dignissimos ducimus qui bl abd gdihtiin praesentium voluptatum deenir rf uiti jllo atque. Dorrupti quos doloregts et quase molestias excepturi sint  hip occaecatib. Rt vero eos et  rewlo jk kjusamu wertilo masdes et iusto odio asgnissme.          <br>
                            <a href="#" class="btn">Plus</a>    
                          </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="bg-2">
        <div class="container_12">
            <div class="grid_7">
            	<div class="border-1">
                    <h2 class="indent-1">About Us</h2>
                    <div class="wrap box-2">
                        <img src="images/page1-img9.jpg" alt="">
                        <p class="text-info"><a href="http://blog.templatemonster.com/free-website-templates/" target="_blank">Click here</a> for more info about this free website template created by TemplateMonster.com </p>
                        <p>Lorem ipsum dolor sit amet consec tetuer adiping elit. Praesent vestibulum molestie lacus. Aenegan nonummy hendrerit mauris. Phasellus portafgtthh Fusce suscipit varius mium sociisileli.</p>
                        Lorem ipsum dolor sit amet, consec tetuer adipiscing elit. Praesent vestibulum molestiealld je lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cumtyle sociis natoque penatibus et  montes nascetur ridiculus mus. Nulla dui. Fusce feue suadatroli odio. Morbi nunc odio, gravida at, cursusnec luctus a lorem. Maecenas tristique orci semertyle. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donecorem ipmolotyweqop
    dolor sit amet  Lorem ipsum dolor sit amet, consec. Fusce suscipit vam.<br>
    					<a href="#" class="btn">more</a>                   
                    </div>
                </div>
            </div>
            <div class="grid_5">
            	<h2 class="indent-1">Latest News</h2>
                <ul class="list-news">
                	<li>
                    	<div class="wrap">
                            <div class="badge">27<span>APR</span></div>
                            <span class="text-info">Duis posuere consectetur pellentesqe;</span><br>
                            <a href="#">April 27.03.12</a>
                        </div>
                        Sed nisi turpis pellentesque at ultrices in dapibus in magnatr. Nunc easi diam risus placerat ut scelerisque et suscipit euter ante. Nullam vitae dolor ullcper felises cursus gravida.                
                    </li>
                    <li>
                    	<div class="wrap">
                            <div class="badge">29<span>APR</span></div>
                            <span class="text-info">Asuis kosuer fronsectetuo ellentesqi;</span><br>
                            <a href="#">April 29.03.12</a>
                        </div>
                        Hoed nisi turpis, pellentesque at ultrices idapibus in magnawe. Lunc easi diam risus, placerat ut scelerisque esuscipit eu ante. Kiullam vitae dolor ullcper felises cursus gravidew.              
                    </li>
                </ul>
            </div>
        </div>
    </div>           
</div>
<!--==============================footer=================================-->
<footer>
	<div class="container_12">
		<div class="grid_8">
			<span>BiZZ &copy; 2013 | Privacy Policy | Website  designed by <a href="http://www.templatemonster.com/" rel="nofollow" target="_blank">TemplateMonster.com</a></span>
		</div>
        <div class="grid_4">
        	<ul class="soc-icon">
            	<li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
                <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
                <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
            </ul>
        </div>
	</div>
</footer>

</body>
</html>