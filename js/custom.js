 $(function() {
	 $('#options_button').click(function() {
                $('#options').slideToggle('fast');
				var width=$(this).css('left')
				if (width=='-2px')
					$(this).css('left', '153px');
				else $(this).css('left', '-2px');
            });
	
	  $('#texture_options img').click(function () {
                   var source='url('+$(this).attr('src')+')';
				     $('body').css('background-image',source);                
                });
				
	$('.color').click(function(){
		var color=$(this).css('background-color');
		$('.color_text').css('color', color);
		$('.color_button').css('background-color', color);
	});
	
});


// Subscribe form
function SubscribeForm(){
	$('#signupform').submit(function(){
		$('.email').removeClass('error')
		$('em.error').remove();
		
		var error = false;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					
		if( $.trim($('.email').val()) == '' ) {
				$(this).append('<em class="error">veuillez entrer votre adresse mail</em>');
				$(this).addClass('error');
				error = true;
			} else if(!emailReg.test(jQuery.trim($('.email').val()))) {
					$(this).append('<em class="error">veuillez entrer une adresse mail valide</em>');
					$(this).addClass('error');
					error = true;
				}						
		if(!error){
			$("#submit", this).after('<span id="form_loading"></span>');
			var formValues = $(this).serialize();
			
			$.post($(this).attr('action'), formValues, function(data){
				$("#signupform").before(data);				
			});
			
			$(':input[type="text"]').attr('value', '');
		}
		return false
	});	
}


